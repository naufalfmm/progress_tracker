'use strict';

var jwt = require('jsonwebtoken');
var moment = require('moment');
var Sequelize = require('sequelize');
var db = require('../services/database'),
    User = require('../models/user');

// The user controller.
var userController = {}

module.exports = userController;