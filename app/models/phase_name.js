// The Module model.
'use strict'; 

var Sequelize = require('sequelize');
var config = require('../config'),
    db = require('../services/database');
// var Phase = require('../models/phase');

// 1: The model schema.
var modelDefinition = {
    phasename_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull:false
    },
    phasename_name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        len: [1,50],
        get() {
            if (this.getDataValue('phasename_name').length > 0) {
                var phasename = this.getDataValue('phasename_name')
                console.log(phasename, phasename[0].toUpperCase())
                return phasename[0].toUpperCase() + phasename.substr(1)
            } else {
                return ""
            }
        }
        // set(val) {
        //     if (val.length >= 0) {
        //         this.setDataValue('phasename_name', val[0].toUpperString() + val.substr(1))
        //     }
        // }  
    }
};

// 2: Define the Module model.
var PhasenameModel = db.define('phase_name', modelDefinition);

PhasenameModel.addHook("afterSync", "addPhaseName", (options) => {
    PhasenameModel.count({where: {phasename_name: "design"}}).then(c => {
        if (c === 0) {
            var newPhasename = {
                phasename_id: 1,
                phasename_name: "design"
            };
            PhasenameModel.create(newPhasename)
        }
    })

    PhasenameModel.count({where: {phasename_name: "coding"}}).then(c => {
        if (c === 0) {
            var newPhasename = {
                phasename_id: 2,
                phasename_name: "coding"
            };
            PhasenameModel.create(newPhasename)
        }
    })

    PhasenameModel.count({where: {phasename_name: "testing"}}).then(c => {
        if (c === 0) {
            var newPhasename = {
                phasename_id: 3,
                phasename_name: "testing"
            };
            PhasenameModel.create(newPhasename)
        }
    })
})

// Phase.belongsTo(PhasenameModel, { foreignKey: {name:'phase_phasename_id', allowNull:true} })

module.exports = PhasenameModel;